# URL shortener

This is an URL shortener service with the following restful api:

method | url | body | content-type | description
--- | --- | --- | --- | ---
*POST* | /create | a long url |  text/plain; charset=utf-8 | registers the long url in the system returning a short url that redirects to the long one
*GET* | shorturl from above | N/A | N/A | will redirect to the registered long url if any

## Installation 

1. Make sure you have scala and sbt installed.
2. Checkout the code: `git clone git@gitlab.com:olle.martensson/urlshortener.git`
3. Download dependencies and compile the code:
    1. `cd urlshortener`
    2. `sbt compile` 

## Usage  

#### Run locally in single node
To test the system locally in a single JVM: 
- `sbt run`
- Register a new url: `curl -X POST -H "Content-Type: text/plain" -d "http://www.google.se" http://localhost:8080/create`
- Use the short url: `curl http://localhost:8080/UkcqOprdVP3po5frejmPLg==`

#### Run integration test on multiple JVM:s
The test is located in [src/multi-jvm/scala/urlshortener/UrlShortenerMultiNodeSpec](https://gitlab.com/olle.martensson/urlshortener/blob/master/src/multi-jvm/scala/urlshortener/UrlShortenerMultiNodeSpec.scala) 
- `sbt multi-jvm:test`

#### Run unit tests
- `sbt test`
   