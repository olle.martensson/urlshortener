package olle.urlshortener

import akka.actor.ActorSystem
import akka.cluster.Cluster

object Application extends App {
  // startup the actorsystem
  val system = ActorSystem()
  // and the cluster
  Cluster(system)
  // and our urlshortener service
  val server = Server(system)
  // shutdown cleanly
  sys.addShutdownHook {
    implicit val ec = scala.concurrent.ExecutionContext.global
    server.bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}
