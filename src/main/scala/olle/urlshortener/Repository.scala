package olle.urlshortener

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Status}
import akka.cluster.ddata.Replicator._
import akka.cluster.ddata.{DistributedData, Replicator, SelfUniqueAddress, _}

object Repository {
  private def DataKeyId = "url-repo"

  def props(config: RepoConfig): Props = Props(classOf[Repository], config)
}

class Repository(config: RepoConfig) extends Actor with ActorLogging {

  import Repository._

  val replicator: ActorRef = DistributedData(context.system).replicator
  implicit val node: SelfUniqueAddress = DistributedData(context.system).selfUniqueAddress
  private val dataKey = LWWMapKey.create[String, String](DataKeyId)
  private val ReadMajority = Replicator.ReadMajority(config.readTimeout)
  private val WriteMajority = Replicator.WriteMajority(config.writeTimeout)

  override def receive: Receive = receivePutUrl
    .orElse(receiveGetUrl)

  def receivePutUrl: Receive = {
    case PutUrl(hash, longUrl) => {
      val update = Update(dataKey, LWWMap.empty[String, String], WriteMajority, Some((sender(), hash, longUrl))) {
        _.put(node, hash, longUrl)
      }
      replicator ! update
    }

    case UpdateSuccess(_, Some((replyTo: ActorRef, hash: String, url: String))) => {
      replyTo ! UrlAdded(hash, url)
    }

    case f: UpdateFailure[_] => {
      f.request foreach {
        case replyTo: ActorRef => replyTo ! Status.Failure(new Exception("Could not perform update"))
      }
    }
  }

  def receiveGetUrl: Receive = {
    case GetUrl(hash) => {
      replicator ! Get(dataKey, ReadMajority, Some((sender(), hash)))
    }

    case res@GetSuccess(_, Some((replyTo: ActorRef, hash: String))) => {
      val url = res.get(dataKey).entries.get(hash)
      replyTo ! GetUrlResponse(url)
    }

    case GetFailure(_, Some((replyTo: ActorRef, _))) => {
      replyTo ! Status.Failure(new Exception("Could not perform get request"))
    }
  }
}
