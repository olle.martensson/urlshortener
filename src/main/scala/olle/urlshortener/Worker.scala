package olle.urlshortener

import java.security.MessageDigest
import java.util.Base64

import akka.actor.{Actor, ActorLogging, Props}

import scala.util.Try

object Worker {
  def encodeURL(url: String): Encoded = {
    val md5 = MessageDigest.getInstance("MD5")
    val b64 = Base64.getEncoder
    val hash = md5.digest(url.getBytes("UTF-8"))
    val shortUrl = b64.encodeToString(hash)

    Encoded(shortUrl)
  }

  // https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
  private val UrlRegex = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)".r

  def validateUrl(url: String): Boolean = url match {
    case UrlRegex(_*) => true
    case _ => false
  }

  def validateHash(hash: String): Boolean = {
    val b64 = Base64.getDecoder
    hash.length == 24 && Try(b64.decode(hash)).isSuccess
  }

  def props: Props = Props[Worker]
}

class Worker extends Actor with ActorLogging {
  import Worker._

  override def receive: Receive = {
    case Encode(url) => sender ! encodeURL(url)
    case ValidateUrl(url) => sender ! Validation(validateUrl(url))
    case ValidateHash(hash) => sender ! Validation(validateHash(hash))
  }

  def props: Props = Props[Worker]
}
