package urlshortener

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import olle.urlshortener.{Encode, Encoded, ValidateHash, ValidateUrl, Validation, Worker}
import org.scalatest.matchers.should.Matchers
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

import scala.util.Random

class WorkerSpec extends TestKit(ActorSystem("MySpec"))
  with ImplicitSender
  with AnyWordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Worker" must {
    val worker = system.actorOf(Worker.props)

    "encode all url:s to the same size" in {
      for(len <- 1 to 1000) {
        val randString = Random.alphanumeric.take(len).mkString
        worker ! Encode(s"http://www.google.se/$randString")
        val encoded = expectMsgType[Encoded]
        assert(encoded.hash.length == 24)
      }
    }

    "verify url:s correctly" in {
      val goodUrl = "https://www.google.se"
      worker ! ValidateUrl(goodUrl)
      expectMsg(Validation(true))

      val badUrl = "www.google.se"
      worker ! ValidateUrl(badUrl)
      expectMsg(Validation(false))
    }

    "verify hash:es correctly" in {
      val goodHash = Worker.encodeURL("whatever").hash
      worker ! ValidateHash(goodHash)
      expectMsg(Validation(true))

      val badHash = "bogus"
      worker ! ValidateHash(badHash)
      expectMsg(Validation(false))
    }
  }
}
