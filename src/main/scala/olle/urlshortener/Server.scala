package olle.urlshortener

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.pattern._
import akka.routing.BalancingPool
import akka.stream.{ActorMaterializer, Materializer}
import akka.http.scaladsl.model._
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object Server {
  def apply(system: ActorSystem): Server = {
    new Server()(system)
  }
}

class Server(implicit system: ActorSystem) {
  private implicit val materializer: Materializer = ActorMaterializer()
  private implicit val executionContext: ExecutionContext = system.dispatcher
  private implicit val timeout: Timeout = Timeout(3.seconds)

  // parse the configuration from application.conf
  val config = Config(system.settings.config)

  // spin up a router and pool of workers that is the size of avail cores since work are cpu-bound.
  // this type of router keeps the backlog for each worker actor as even as possible
  // by doing work stealing
  private val numCores = Runtime.getRuntime.availableProcessors()
  private val workers = system.actorOf(BalancingPool(numCores).props(Props[Worker]), "worker-pool")

  // start the persistence actor
  private val repo = system.actorOf(Repository.props(config.repo))

  // fail fast if we run into problems and protect the system
  // if the workers fail or to loaded
  private val breaker = new CircuitBreaker(
    system.scheduler,
    maxFailures = config.circuitBreaker.maxFailures,
    callTimeout = config.circuitBreaker.callTimeout,
    config.circuitBreaker.resetTimeout,
  ).onOpen(onCicuitBreakerOpen())

  // setup the REST API
  private val routes =
    path("create") {
      post {
        entity(as[String]) { url =>
          onCompleteWithBreaker(breaker)(validateUrl(url)) {
            case Success(true) => {
              onCompleteWithBreaker(breaker)(encodeAndStoreLongUrl(url)) {
                case Success(hash: String) => complete(s"http://${config.http.host}:${config.http.port}/$hash")
                case Failure(ex) => complete(StatusCodes.InternalServerError, s"An error occurred: ${ex.getMessage}")
              }
            }
            case _ => complete(StatusCodes.BadRequest, "Not a valid URL")
          }
        }
      }
    } ~ path(Segment) { hash =>
      get {
        onCompleteWithBreaker(breaker)(validateHash(hash)) {
          case Success(true) => {
            onCompleteWithBreaker(breaker)(decodeAndLookupShortUrl(hash)) {
              case Success(Some(longUrl)) => redirect(longUrl, StatusCodes.PermanentRedirect)
              case Success(None) => complete(StatusCodes.NotFound, "The url was not found")
              case Failure(ex) => complete(StatusCodes.InternalServerError, s"An error occurred: ${ex.getMessage}")
            }
          }
          case _ => complete(StatusCodes.BadRequest, "Not a valid hash")
        }
      }
    }

  // start the http server
  val bindingFuture: Future[Http.ServerBinding] =
    Http().bindAndHandle(routes, config.http.address, config.http.port)
  println(s"Server online at http://${config.http.address}:${config.http.port}/")

  def validateUrl(url: String): Future[Boolean] =
    workers.ask(ValidateUrl(url)).mapTo[Validation].map(_.isValid)

  def encodeAndStoreLongUrl(longUrl: String): Future[String] = for {
    Validation(isValid) <- workers.ask(ValidateUrl(longUrl)).mapTo[Validation] if isValid
    Encoded(hash) <- workers.ask(Encode(longUrl)).mapTo[Encoded]
    _ <- repo.ask(PutUrl(hash, longUrl))
  } yield hash

  def validateHash(hash: String): Future[Boolean] =
    workers.ask(ValidateHash(hash)).mapTo[Validation].map(_.isValid)

  def decodeAndLookupShortUrl(hash: String): Future[Option[String]] =
    repo.ask(GetUrl(hash)).mapTo[GetUrlResponse].map(_.url)

  private def onCicuitBreakerOpen(): Unit = {
    system.log.warning(s"Circuit breaker is open and will not close for: ${config.circuitBreaker.resetTimeout}")
  }
}

