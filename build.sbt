import com.typesafe.sbt.SbtMultiJvm.multiJvmSettings

ThisBuild / scalaVersion := "2.13.1"
ThisBuild / organization := "olle.urlshortener"

lazy val hello = (project in file("."))
  .enablePlugins(MultiJvmPlugin)
  .configs(MultiJvm)
  .settings(
    name := "urlshortener",
    parallelExecution in Test := false,
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.11",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.6.1",
    libraryDependencies += "com.typesafe.akka" %% "akka-distributed-data" % "2.6.1",
    libraryDependencies += "com.typesafe.akka" %% "akka-serialization-jackson" % "2.6.1",
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.0",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test",
    libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.6.1" % "test",
    libraryDependencies += "com.typesafe.akka" %% "akka-multi-node-testkit" % "2.6.1" % "test",
    mainClass in(Compile, run) := Some("olle.urlshortener.Application")
  )
