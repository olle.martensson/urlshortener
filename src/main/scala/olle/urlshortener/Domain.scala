package olle.urlshortener

sealed trait Domain // marker for serialization framework

sealed trait RepoMessage extends Domain
sealed trait RepoCmd extends RepoMessage
sealed trait RepoEvt extends RepoMessage
case class PutUrl(b64Hash: String, longUrl: String) extends RepoCmd
case class GetUrl(b64Hash: String) extends RepoCmd
case class UrlAdded(hash: String, longUrl: String) extends RepoEvt
case class GetUrlResponse(url: Option[String]) extends RepoMessage
case class RepoState(urls: Map[String, String], size: Int) extends RepoMessage

sealed trait WorkerMessage extends Domain
sealed trait WorkerReq extends WorkerMessage
sealed trait WorkerRes extends WorkerMessage
case class Encode(url: String) extends WorkerReq
case class Encoded(hash: String) extends WorkerRes
case class ValidateUrl(url: String) extends WorkerReq
case class Validation(isValid: Boolean) extends WorkerRes
case class ValidateHash(hash: String) extends WorkerRes