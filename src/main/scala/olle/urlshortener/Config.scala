package olle.urlshortener

import scala.concurrent.duration._

object Config {

  implicit def asFiniteDuration(d: java.time.Duration): FiniteDuration =
    scala.concurrent.duration.Duration.fromNanos(d.toNanos)

  def apply(config: com.typesafe.config.Config): Config = {
    Config(
      HttpConfig(
        config.getInt("urlshortener.http.port"),
        config.getString("urlshortener.http.address"),
        config.getString("urlshortener.http.host")
      ),
      RepoConfig(
        config.getDuration("urlshortener.repo.read-timeout"),
        config.getDuration("urlshortener.repo.read-timeout")
      ),
      CircuitBreakerConfig(
        config.getInt("urlshortener.circuit-breaker.max-failures"),
        config.getDuration("urlshortener.circuit-breaker.call-timeout"),
        config.getDuration("urlshortener.circuit-breaker.reset-timeout"),
      )
    )
  }
}

case class Config(http: HttpConfig, repo: RepoConfig, circuitBreaker: CircuitBreakerConfig)
case class HttpConfig(port: Int, address: String, host: String)
case class RepoConfig(readTimeout: FiniteDuration, writeTimeout: FiniteDuration)
case class CircuitBreakerConfig(maxFailures: Int, callTimeout: FiniteDuration, resetTimeout: FiniteDuration)