package olle.urlshortener

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpRequest, HttpResponse}

import scala.concurrent.Future


object Client {
  def  postLongUrl(url: String, host: String, port: Int)(implicit system: ActorSystem): Future[HttpResponse] = {
    val req = HttpRequest(
      method = HttpMethods.POST,
      uri = s"http://$host:$port/create",
      entity = HttpEntity(url)
    )
    Http().singleRequest(req)
  }

  def getLongUrl(url: String)(implicit system: ActorSystem): Future[HttpResponse] = {
    val req = HttpRequest(method = HttpMethods.GET, uri = url)
    Http().singleRequest(req)
  }
}
