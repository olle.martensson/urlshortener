package urlshortener

import akka.cluster.Cluster
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.model.{HttpResponse, StatusCodes, Uri}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.remote.testkit.MultiNodeSpec
import olle.urlshortener.{Client, Server, Worker}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}

import scala.concurrent.duration._

class UrlShortenerMultiNodeSpec extends MultiNodeSpec(UrlShortenerMultiNodeConfig)
  with ScalaTestMultiNodeSpec with ScalaFutures {

  import UrlShortenerMultiNodeConfig._

  override def initialParticipants: Int = 3

  override implicit val patienceConfig: PatienceConfig =
    PatienceConfig(scaled(Span(15, Seconds)))

  var serverNode1: Option[Server] = None
  var serverNode2: Option[Server] = None
  var serverNode3: Option[Server] = None

  val googleUrl = "http://www.google.se"
  val googleHash: String = Worker.encodeURL(googleUrl).hash

  "A Url Shortener System" must {
    "start all nodes" in within(15.seconds) {

      Cluster(system).subscribe(testActor, classOf[MemberUp])

      expectMsgClass(classOf[CurrentClusterState])

      Cluster(system) join node(node1).address

      runOn(node1) {
        serverNode1 = Some(Server(system))
      }

      runOn(node2) {
        serverNode2 = Some(Server(system))
      }

      runOn(node3) {
        serverNode3 = Some(Server(system))
      }

      receiveN(3).collect { case MemberUp(m) => m.address }.toSet must be(
        Set(node(node1).address, node(node2).address, node(node3).address)
      )

      testConductor.enter("all-up")
    }

    "be able to hash and store an url on all nodes" in within(5.seconds) {
      val longUrl = "https://www.google.se/okidoki"
      val hash: String = Worker.encodeURL(longUrl).hash

      runOn(node1) {
        serverNode1 foreach { n1 =>
          n1.encodeAndStoreLongUrl(longUrl).futureValue mustBe hash
          enterBarrier("url-registered")
        }
      }

      runOn(node2) {
        serverNode2 foreach { n2 =>
          enterBarrier("url-registered")
          n2.decodeAndLookupShortUrl(hash).futureValue mustBe Some(longUrl)
        }
      }

      runOn(node3) {
        serverNode3 foreach { n3 =>
          enterBarrier("url-registered")
          n3.decodeAndLookupShortUrl(hash).futureValue mustBe Some(longUrl)
        }
      }
    }

    "be able to hash and store an url using rest api on all nodes" in {
      runOn(node1) {
        val res = postLongUrl(serverNode1, googleUrl)
        Unmarshal(res.entity).to[String].futureValue mustBe
          s"http://${serverNode1.get.config.http.host}:${serverNode1.get.config.http.port}/${googleHash}"
        enterBarrier("google-url-registered")
      }

      runOn(node2) {
        enterBarrier("google-url-registered")
        val res = getLongUrl(serverNode2, googleHash)
        res.status mustBe StatusCodes.PermanentRedirect
        res.headers.contains(Location(Uri(googleUrl)))
      }

      runOn(node3) {
        enterBarrier("google-url-registered")
        val res = getLongUrl(serverNode3, googleHash)
        res.status mustBe StatusCodes.PermanentRedirect
        res.headers.contains(Location(Uri(googleUrl)))
      }
    }

    "create should return 400 badreq on bad url" in {
      runOn(node1) {
        val res = postLongUrl(serverNode1, "google.se")
        res.status mustBe StatusCodes.BadRequest
      }
    }

    "lookup should return 400 badreq on bad hash" in {
      runOn(node2) {
        val res = getLongUrl(serverNode2, "badhash")
        res.status mustBe StatusCodes.BadRequest
      }
    }

    "lookup should return 404 notfound on not registered url" in {
      runOn(node3) {
        val res = getLongUrl(serverNode3, Worker.encodeURL("https://www.arstechnica.com").hash)
        res.status mustBe StatusCodes.NotFound
      }
    }
  }

  def postLongUrl(serverOption: Option[Server], url: String): HttpResponse = {
    val server = serverOption.get
    Client.postLongUrl(url, server.config.http.host, server.config.http.port).futureValue
  }

  def getLongUrl(serverOption: Option[Server], hash: String): HttpResponse = {
    val server = serverOption.get
    Client.getLongUrl(s"http://${server.config.http.host}:${server.config.http.port}/$hash").futureValue
  }
}

class UrlShortenerMultiJvmNode1 extends UrlShortenerMultiNodeSpec
class UrlShortenerMultiJvmNode2 extends UrlShortenerMultiNodeSpec
class UrlShortenerMultiJvmNode3 extends UrlShortenerMultiNodeSpec
