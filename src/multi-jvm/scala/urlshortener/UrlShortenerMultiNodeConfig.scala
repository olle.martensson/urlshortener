package urlshortener

import akka.remote.testkit.MultiNodeConfig
import com.typesafe.config.ConfigFactory

object UrlShortenerMultiNodeConfig extends MultiNodeConfig {
  // define the test roles that need to be filled when running the test
  // in short, we define a set of nodes that we want to work with
  val node1 = role("node1")
  val node2 = role("node2")
  val node3 = role("node3")

  // enable the test transport that allows to do fancy things such as blackhole, throttle, etc.
  testTransport(on = true)

  // configuration for node1
  nodeConfig(node1)(ConfigFactory.parseString(
    """
      |akka.cluster.roles=[]
    """.stripMargin))

  // configuration for node2
  nodeConfig(node2)(ConfigFactory.parseString(
    """
      |urlshortener.http.port = 8081
      |akka.cluster.roles=[]
    """.stripMargin))

  // configuration for node3
  nodeConfig(node3)(ConfigFactory.parseString(
    """
      |urlshortener.http.port = 8082
      |akka.cluster.roles=[]
    """.stripMargin))

  // common configuration for all nodes
  commonConfig(ConfigFactory.parseString(
    """
      |akka.loglevel=INFO
      |akka.actor.provider = cluster
      |akka.remote.artery.enabled = on
      |akka.coordinated-shutdown.run-by-jvm-shutdown-hook = off
      |akka.coordinated-shutdown.terminate-actor-system = off
      |akka.cluster.run-coordinated-shutdown-when-down = off
      |akka.actor {
      |  serialization-bindings {
      |    "olle.urlshortener.Domain" = jackson-json
      |  }
      |}
      |urlshortener {
      |  http {
      |    address = 0.0.0.0
      |    port = 8080
      |    host = localhost
      |  }
      |  repo {
      |    read-timeout = 1 second
      |    write-timeout = 2 seconds
      |  }
      |  circuit-breaker {
      |    max-failures = 1
      |    call-timeout = 3 seconds
      |    reset-timeout = 1 second
      |  }
      |}
    """.stripMargin))
}
